package main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import dominio.Aguila;
import dominio.Animal;
import dominio.Leon;
import dominio.Periquito;
import dominio.Tigre; //mi tigresoooon bonicooo

public class Main {

	public static void main(String[] args) throws IOException {

		// BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		
		Animal periquito = new Periquito();
		Animal leon = new Leon();
		Animal aguila = new Aguila();
		Animal tigre = new Tigre();

		List<Animal> animales = new ArrayList<Animal>();
		animales.add(periquito);
		animales.add(leon);
		animales.add(aguila);
		animales.add(tigre);

		for (Animal animal : animales) {
			System.out.println(animal.datosAnimal() + animal.getAlimento());
		}

	}

}
