package dominio;

public class Periquito extends Animal {

	protected String habilidad;

	private int IdPeriquito;

	private static int nextId = 1;

	public Periquito() {
		super();
		setNombre("Periquito");
		setTipo("Ave");
		habilidad = ("Volar");
		setAlimento("Gusanos");
		IdPeriquito = nextId;
		nextId++;
		setNumAnimales(IdPeriquito);

	}

}
