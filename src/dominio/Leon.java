package dominio;

public class Leon extends Animal {

	private String habilidad;

	private int IdLeon;

	private static int nextId = 1;

	public Leon() {
		super();
		setNombre("Leon");
		setTipo("Mamifero");
		habilidad = "Galopar";
		setAlimento("Carne");
		IdLeon = nextId;
		nextId++;
		setNumAnimales(IdLeon);

	}

}
