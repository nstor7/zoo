package dominio;

public class Tigre extends Animal {

	private String habilidad;

	private int IdTigre;

	private static int nextId = 1;

	public Tigre() {
		super();
		setNombre("Tigre");
		setTipo("Mamifero");
		habilidad = "Zarpazo";
		setAlimento("Cebras");
		IdTigre = nextId;
		nextId++;
		setNumAnimales(IdTigre);

	}

}
