package dominio;

public class Animal {

	private int numAnimales;

	private String nombre;

	private String alimento;

	private String tipo;

	public String recibirAlimento() {

		return "Me encanta comer: " + alimento;
	}

	public String datosAnimal() {
		return "N�:" + numAnimales + " Soy un " + nombre + " de tipo " + tipo + " y ";
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getAlimento() {
		return "Me encanta comer " + alimento;
	}

	public void setAlimento(String alimento) {
		this.alimento = alimento;
	}

	public int getNumAnimales() {
		return numAnimales;
	}

	public void setNumAnimales(int numAnimales) {
		this.numAnimales = numAnimales;
	}

}
