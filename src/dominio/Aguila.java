package dominio;

public class Aguila extends Animal {

	private String habilidad;

	private int IdAguila;

	private static int nextId = 1;

	public Aguila() {
		super();
		setNombre("Aguila");
		setTipo("Ave");
		habilidad = "Desgarrar";
		setAlimento("Conejos");
		IdAguila = nextId;
		nextId++;
		setNumAnimales(IdAguila);

	}

}
